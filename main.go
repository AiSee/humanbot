package main

import (
	"bitbucket.org/AiSee/minilog"
	"encoding/json"
	"github.com/chippydip/go-sc2ai/api"
	"github.com/chippydip/go-sc2ai/client"
	"github.com/chippydip/go-sc2ai/runner"
	"io/ioutil"
	"strings"
	"time"
)

type Config struct {
	Fps  float64 `json:"fps"`
	Race string  `json:"race"`
}

var config Config

func runAgent(info client.AgentInfo) {
	frameDelay := 1000000000 / config.Fps
	nanoDelay := int64(frameDelay)
	for info.IsInGame() {
		start := time.Now()
		if err := info.Step(1); err != nil {
			log.Error(err)
			if strings.Contains(err.Error(), "An existing connection was forcibly closed by the remote host.") {
				return
			}
		}
		delta := time.Now().Sub(start).Nanoseconds()
		time.Sleep(time.Duration(nanoDelay-delta) * time.Nanosecond)
	}
}

func main() {
	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &config)
	if err != nil {
		panic(err)
	}
	race := api.Race_Random
	switch config.Race {
	case "terran":
		race = api.Race_Terran
	case "zerg":
		race = api.Race_Zerg
	case "protoss":
		race = api.Race_Protoss
	}

	runner.Set("map", "Port Aleksander LE.SC2Map") // DarknessSanctuaryLE
	runner.Set("ComputerOpponent", "true")
	runner.Set("ComputerRace", "random")
	runner.Set("ComputerDifficulty", "CheatMoney") // CheatInsane CheatMoney VeryHard
	// runner.Set("realtime", "true")

	// Create the agent and then start the game
	runner.RunAgent(client.NewParticipant(race, client.AgentFunc(runAgent), ""))
}
